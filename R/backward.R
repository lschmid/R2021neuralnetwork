#' Backward propagation
#'
#' executes a backward propagation through a given neural network with the true
#' output of some training data
#'
#' @param nn Neural Network: net containing the fed forward values
#' @param labels_true Matrix: (y_true) numeric true data labels: desired output vector
#' @param loss Character (vector): Loss function to use. "CE" for Cross Entroy loss
#' and "MSE" for Mean Squared Error
#'
#' @return list of matrices containing the gradient of the loss function with
#' respect to the weights
#'
#' @examples
#' backward(nn, 4, lossfunction)
#' backward(nn, c(0, 0, 0, 1, 0, 0), lossfunction)
#'

backward <- function(nn, labels_true, loss = "MSE")
{

  #labels <- c(1,0,0,0,0,0,0,0,0,0)

  #get the derivative of the selected loss function
  d_loss <- loss_derivative(loss)
  #load important values from the neural network object
  structure <- slot(nn, "structure")
  values <- slot(nn, "values")
  values_before_activation <- slot(nn, "values_before_activation")
  weights <- slot(nn, "weights")
  activation_derivatives <- slot(nn, "activation_derivative")
  n_layers <- length(structure)


  #compute the gradient at which neurons in the last layer affect the cost function.
  node_gradients <- d_loss(labels_true, slot(nn, "values")[[n_layers]])

  #create an empty list that contains the weight gradients to be calculated.
  weight_gradients <- create_empty_weight_structure(structure)
  #traverse through the network layer by layer backwards.
  for (layer in n_layers:2)
  {
    n_nodes <- structure[[layer]]
    n_prior_nodes <- structure[[layer - 1]]

    #load the derivate of the activation function for the current layer
    d_activation <- activation_derivatives[[layer - 1]]

    #create an empty vector that cummulates the gradients from all later nodes.
    prior_node_gradients <- rep(0, n_prior_nodes)

    #go through every node in the current layer
    for (node in 1:n_nodes)
    {
      node_value_before_activation <-
        values_before_activation[[layer - 1]][node]
      # describes how input of the activation function affects the node value.
      activation_gradient <-
        d_activation(node_value_before_activation)
      # activation loss gradient describes how the input of the activation
      # function affects the cost function.
      activation_loss_gradient <-
        activation_gradient *  node_gradients[[node]]

      #go through every node in the previous layer.
      for (prior_node in 1:n_prior_nodes)
      {
        prior_node_value <- values[[layer - 1]][prior_node]
        weight_value <- weights[[layer - 1]][prior_node, node]

        # amount by which a weight affects the cost function equals the prior
        # node value times the activation loss gradient.
        weight_gradient <-
          prior_node_value * activation_loss_gradient
        # amount by which the prior node value affects the cost function
        # over given weight equals its weight times the activation loss gradient.
        individual_prior_node_gradient <-
          weight_value * activation_loss_gradient

        # set according position in the gradient array to the calculated value.
        weight_gradients[[layer - 1]][prior_node, node] <-
          weight_gradient
        #add the individual node gradient to the total gradient of a node.
        prior_node_gradients[[prior_node]] <-
          prior_node_gradients[[prior_node]] + individual_prior_node_gradient
      }
      # calculate bias gradient by assuming a node value of 1.
      bias_gradient <- 1 * activation_loss_gradient

      # store at the appropriate position.
      weight_gradients[[layer - 1]][n_prior_nodes + 1, node] <-
        bias_gradient
    }
    # after finishing layer: set the current node gradient to the
    # calculated gradient of the next layer.
    node_gradients <- prior_node_gradients
  }

  #return the calculated weight gradients.
  weight_gradients
}
