

#' Shiny App to illustrate a neural network
#'
#' Function to create a neural net using the shiny app. Calling
#' \code{shinynet( )} renders a shiny app where it is possible to create and
#' train a custom made neural network. Commonly used to illustrate the mnist
#' handwritten data set.
#'
#' @return ShinyApp
#'
#' @seealso
#' \code{\link{NeuralNet}} for creating a neural network.
#'
#' \code{\link{train_nn}} for training a neural network.
#'
#' \code{\link{predict_nn}} for predicting a neural network.
#'
#' \code{\link{test_nn}} for testing a neural network.
#'
#' \code{\link{mnist}} for information about the MNIST dataset.
#'
#' @examples shinynet()
#' @export

## Only run examples in interactive R sessions

shinynet <- function() {
  if (interactive()) {
    options(device.ask.default = FALSE)

    # Define UI
    ui <- fluidPage(

      # Application title
      titlePanel("Creating - Training and Predicting a Neural Network!"),

      fluidRow(


        # Sidebar with a slider input
        column(3,
               new_nn_name <- "my_neural_net",

               checkboxInput("use_mnist", "Use MNIST", value = FALSE),
               uiOutput("nn_to_train"),

               # Custom data input
               uiOutput("training_data"),
               uiOutput("labels_true"),

               uiOutput("validate_data"),
               uiOutput("labels_validate"),

               uiOutput("test_data"),
               uiOutput("labels_test"),


               # hide input and output layer if MNIST
               uiOutput("show_input_layer"),
               uiOutput("show_output_layer"),


               textOutput("show_hidden_layers11_text"),
               uiOutput("show_hidden_layers11"),

               numericInput("hidden_layers",
                            "Hidden Layers",
                            min = 0,
                            max = NA,
                            value = 1,
                            step = 1
               ),


               uiOutput("show_hidden_layers1"),
               uiOutput("show_hidden_layers2"),
               uiOutput("show_hidden_layers3"),
               uiOutput("show_hidden_layers4"),
               uiOutput("show_hidden_layers5"),
               uiOutput("show_hidden_layers6"),
               uiOutput("show_hidden_layers7"),
               uiOutput("show_hidden_layers8"),
               uiOutput("show_hidden_layers9"),
               uiOutput("show_hidden_layers10"),


               uiOutput("show_hidden_activation"),


               selectInput("activation_fun_output",
                           "Activation Function Output",
                           choices = c("relu", "tanh", "identity", "sigmoid", "softmax"),
                           selected= c("softmax"),
                           multiple = FALSE
               ),


               sliderInput(
                 "weight_init", "Weight Init", min = 0, max = 1,
                 value = 0.3, step = 0.001
               ),


               textInput(
                 "new_nn", "Name to store the Neural Network:",
                 value = "my_neural_net"),
               actionButton("create_nn", "Create Neural Network"),

        ),

        column(3,
               selectInput("optimizer", "Optimizer",
                           choices=list("Gradient Descent"="GD",
                                        "Stochastic Gradient Descent"= "SGD",
                                        "ADAM"="ADAM"),
                           selected="ADAM",
                           multiple = FALSE),
               fluidRow(

                 column(6,
                        numericInput("epochs",
                                     "Epochs: \n",
                                     min = 2,
                                     max = NA,
                                     30,
                                     step = 1
                        )
                 ),

                 column(6,
                        numericInput("batch_size",
                                     "Batch Size:",
                                     min = 1,
                                     max = NA,
                                     value = 128,
                                     step = 1
                        )
                 ),

                 column(6,
                        numericInput("learning_rate",
                                     "Lernrate:",
                                     min = 0,
                                     max = NA,
                                     value = 0.01,
                                     step = 0.001
                        )
                 ),

                 column(6,
                        numericInput("gamma",
                                     "Gamma: \n",
                                     min = 0,
                                     max = NA,
                                     value = 0.01,
                                     step = 0.001
                        )
                 )
               ),
               numericInput("testing_sample_size",
                            "Anzahl Test Datenpunkte",
                            min = 0,
                            max = NA,
                            value = 500,
                            step = 10
               ),
               numericInput("early_stopping_threshold",
                            "Anzahl Schritte Abbruchkriterium",
                            min = 5,
                            max = NA,
                            value = 10,
                            step = 1
               ),
               selectInput("loss_function",
                           "Loss Function",
                           choices = c("MSE", "CE"),
                           multiple = FALSE
               ),

               actionButton("train_nn", "Train Neural Network"),
               actionButton("predict_nn", "Predict Neural Network"),
               actionButton("test_nn", "Test Neural Network"),

        ),

        # Show a plot of the generated distribution
        column(6,
               span(h3(textOutput("TrainInfo")), style="color:blue"),
               plotOutput("loss_accuracy"),
               fluidRow(
                 column(6,
                        plotOutput("digit", height = "310px", width = "110%",)

                 ),
                 column(6,
                        h3(textOutput("prediction")),
                        h3(textOutput("testing"))
                 )
               )
        )
      )
    )



    # Server logic
    server <- function(input, output) {

      #-----------------
      # Create NN
      #-----------------
      observeEvent(input$create_nn, {
        new_nn_name <<- input$new_nn

        layer_vec <- c(
          input$hidden_layers1,
          input$hidden_layers2,
          input$hidden_layers3,
          input$hidden_layers4,
          input$hidden_layers5,
          input$hidden_layers6,
          input$hidden_layers7,
          input$hidden_layers8,
          input$hidden_layers9,
          input$hidden_layers10,
          input$hidden_layers11
        )

        layer_vec <- layer_vec[!is.na(layer_vec)]
        layer_vec <- layer_vec[layer_vec != 0]

        # Recycle Activation function to correct length
        if(length(layer_vec) >= 1){
          # create recycled activation function character vector
          activ_fun_recycle <- rep(input$activation_fun_hidden,
                                   length.out = length(layer_vec))
          activation_fun <- c(activ_fun_recycle,
                              input$activation_fun_output)
          cat(activation_fun, " shiny actiation \n")
        }
        else activation_fun <- input$activation_fun_output

        # create net and write to global environment
        assign(
          new_nn_name,
          NeuralNet(
            input_nodes = as.double(input$input_nodes),
            hidden_layers = as.double(layer_vec),
            output_nodes = as.double(input$output_nodes),
            weight_init = input$weight_init,
            activation_fun = activation_fun
          ),
          .GlobalEnv
        )

        output$nn_to_train = renderUI({
          textInput(
            "nn_to_train", "Neural Network to be trained:",
            value = new_nn_name)

        })
      })

      output$TrainInfo <- renderText("Training the neural network...")


      #-----------------
      # TRAINING
      #-----------------
      observeEvent(input$train_nn, {
        nn_to_train_name <- as.name(input$nn_to_train)

        # Read in MNIST data
        if (input$use_mnist == TRUE) {
          training_data <- normalize_nn(mnist[1:50000, -ncol(mnist)])
          labels_true <- mnist[1:50000, ncol(mnist)]
          validate_data <-  normalize_nn(mnist[50001:60000, -ncol(mnist)])
          labels_validate <-  mnist[50001:60000, ncol(mnist)]
        }
        else { # read in data
          training_data <- eval(as.name(input$training_data))
          labels_true <- eval(as.name(input$labels_true))
          validate_data <- eval(as.name(input$validate_data))
          labels_validate <- eval(as.name(input$labels_validate))
        }

        # training the network and store in global environment
        set.seed(1234)
        tmp_nn <- train_nn(
          nn = eval(nn_to_train_name),
          training_data = training_data,
          labels_true = labels_true,
          validate_data = validate_data,
          labels_validate = labels_validate,
          epochs = input$epochs,
          batch_size = input$batch_size,
          loss_function = input$loss_function,
          learning_rate = input$learning_rate,
          testing_sample_size = input$testing_sample_size,
          optimizer = input$optimizer,
          gamma = input$gamma,
          early_stopping_threshold = input$early_stopping_threshold,
          output_plot = TRUE
        )
        # Assign to network
        assign(
          as.character(nn_to_train_name), tmp_nn$nn, .GlobalEnv
        )

        # Get loss and accuracy to plot
        loss_history <- tmp_nn$loss_history
        accuracy_history <- tmp_nn$accuracy_history

        dat <- tibble(epochs = 1:length(loss_history),
                      loss = loss_history,
                      accuracy = accuracy_history)


        output$loss_accuracy <- renderPlot({

          ggplot(data = dat, mapping = aes(x = epochs)) +
            ggtitle("Loss and accuracy during training") +
            labs(x="epochs", y="loss/ accuracy") +
            geom_line(aes(y = loss), color="black", size = 1.5) +
            geom_point(aes(y = loss), color="black", size = 2.5) +
            geom_line(aes(y = accuracy), color="red", size = 1.5) +
            geom_point(aes(y = accuracy), color="red", size = 2.5)

        })
      })



      #-------------------
      # PREDICTING
      #-------------------
      observeEvent(input$predict_nn, {
        nn_to_train_name <- as.name(input$nn_to_train)

        if (input$use_mnist == TRUE) {
          test_data <-  normalize_nn(mnist[60001:70000, -ncol(mnist)])
          labels_test <-  mnist[60001:70000, ncol(mnist)]
        }
        else { # read in data
          test_data <- eval(as.name(input$test_data))
          labels_test <- eval(as.name(input$labels_test))
        }

        if (input$use_mnist == TRUE) {
          sample_id <- sample(1:10000, 1)
          test_sample <- test_data[sample_id, ]
        }
        else {
          test_sample <- eval(as.name(input$test_data))
        }
        pred <- predict_nn(eval(nn_to_train_name), test_sample) - 1


        # Plotting true Mnist number
        if (input$use_mnist == TRUE) {
          output$digit <- renderPlot(show_digit(test_sample))
        }

        # Naming prediction
        #output$prediction  <- renderText(prediction)
        output$prediction  <- renderText({paste("Prediction of the network: ",pred)})

      })


      #-------------------
      # Testing
      #-------------------
      observeEvent(input$test_nn, {

        nn_to_train_name <- as.name(input$nn_to_train)

        if (input$use_mnist == TRUE) {
          test_data <-  normalize_nn(mnist[60001:70000, -ncol(mnist)])
          labels_test <-  mnist[60001:70000, ncol(mnist)]
        }
        else { # read in data
          test_data <- eval(as.name(input$test_data))
          labels_test <- eval(as.name(input$labels_test))
        }

        test_loss_acc <- test_nn(eval(nn_to_train_name),
                                 test_data,
                                 labels_test,
                                 nrow(test_data))
        loss_str <- paste("Loss =  ", round(test_loss_acc[1], 3), " &  ")
        acc_str <- paste("Accuracy = ", round(test_loss_acc[2], 3))

        # Naming prediction
        #output$prediction  <- renderText(prediction)
        output$testing  <- renderText(
          {paste(loss_str, acc_str, sep = "\n")})

      })


      #--------------------------------
      # Printing to
      output$nn_to_train = renderUI({
        textInput(
          "nn_to_train", "Neural Network to be trained:", value = new_nn_name)
      })

      # Training data
      output$training_data = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("training_data", "Training Data:", value = "train_data")
        }
        else
          NULL
      })
      output$labels_true = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("labels_true", "True Labels:", value = "labels_true")
        }
        else
          NULL
      })

      # Validation data
      output$validate_data = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("validate_data", "Validate Data:", value = "validate_data")
        }
        else
          NULL
      })
      output$labels_validate = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("labels_validate", "Validate Labels:",
                    value = "labels_validate")
        }
        else
          NULL
      })

      # Testing data
      output$test_data = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("test_data", "Testing Data:", value = "test_data")
        }
        else
          NULL
      })
      output$labels_test = renderUI({
        if (input$use_mnist == FALSE) {
          textInput("labels_test", "Testing Labels:",
                    value = "labels_test"
          )
        }
        else
          NULL
      })


      output$show_input_layer = renderUI({
        if (input$use_mnist == FALSE) {
          numericInput("input_nodes",
                       "Input Nodes",
                       min = 1,
                       max = NA,
                       value = 784,
                       step = 1
          )
        }
        else
          NULL
      })

      output$show_output_layer = renderUI({
        if (input$use_mnist == FALSE) {
          numericInput("output_nodes",
                       "Output Nodes",
                       min = 1,
                       max = NA,
                       value = 10,
                       step = 1
          )
        }
        else
          NULL
      })

      output$show_hidden_activation = renderUI({
        if (input$hidden_layers > 0) {
          selectInput("activation_fun_hidden",
                      "Activation Function Hidden",
                      choices = c("relu", "identity", "tanh", "sigmoid"),
                      selected= c("relu"),
                      multiple = TRUE
          )
        }
        else
          NULL
      })

      output$show_hidden_layers1 = renderUI({
        if (input$hidden_layers > 0) {
          numericInput("hidden_layers1",
                       "Nodes HL 1",
                       min = 1,
                       max = NA,
                       value = 32,
                       step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers2 = renderUI({
        if (input$hidden_layers > 1) {
          numericInput("hidden_layers2",
                       "Nodes HL 2",
                       min = 1,
                       max = NA,
                       value = 32,
                       step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers3 = renderUI({
        if (input$hidden_layers > 2) {
          numericInput(
            "hidden_layers3",
            "Nodes HL 3",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers4 = renderUI({
        if (input$hidden_layers > 3) {
          numericInput(
            "hidden_layers4",
            "Nodes HL 4",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers5 = renderUI({
        if (input$hidden_layers > 4) {
          numericInput(
            "hidden_layers5",
            "Nodes HL 5",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers6 = renderUI({
        if (input$hidden_layers > 5) {
          numericInput(
            "hidden_layers6",
            "Nodes HL 6",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers7 = renderUI({
        if (input$hidden_layers > 6) {
          numericInput(
            "hidden_layers7",
            "Nodes HL 7",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers8 = renderUI({
        if (input$hidden_layers > 7) {
          numericInput(
            "hidden_layers8",
            "Nodes HL 8",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers9 = renderUI({
        if (input$hidden_layers > 8) {
          numericInput(
            "hidden_layers9",
            "Nodes HL 9",
            min = 1,
            max = NA,
            value = 8,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers10 = renderUI({
        if (input$hidden_layers > 9) {
          numericInput(
            "hidden_layers10",
            "Nodes HL 10",
            min = 1,
            max = NA,
            value = NULL,
            step = 1
          )
        }
        else
          NULL
      })
      output$show_hidden_layers11 = renderUI({
        if (input$hidden_layers > 10) {
          textInput("hidden_layers11", "Additional HL" , value = "c(2, 5, 10)")
        }
        else
          NULL
      })
      output$show_hidden_layers11_text  <- renderText({
        if (input$hidden_layers > 10) {
          "Please insert additional hidden layers as a numeric vector \n with the
            nodes amount per level: c(nodes l1, nodes l2,...)"
        }
        else
          NULL
      })

    }

    # Complete app with UI and server components
    shinyApp(ui, server)
  }
}


