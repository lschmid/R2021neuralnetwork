# Zufallsgrößen für Test-Netzwerk
n_hidden <- sample(2:8, 1)
test_hidden_layers <- sample(4:16, n_hidden)
test_weights <- runif(1, 0, 1)

test_test_net <- NeuralNet(1, input_data = NULL, test_hidden_layers, 1, test_weights)

# Test Daten generieren mit Cosinus
input_test <- as.matrix(runif(1000, 0, 2*pi))
labels_test <- as.matrix(cos(input_test))

# Validierungsdaten generieren mit Cosinus
input_test_validate <- as.matrix(runif(100, 0, 2*pi))
labels_test_validate <- as.matrix(cos(input_test_validate))

#train
trained_net_testing <- train_nn(test_test_net, input_test, labels_test, input_test_validate, labels_test_validate,
                        learning_rate = 0.01,
                        epochs = 16,
                        batch_size = 128,
                        testing_sample_size = 100,
                        optimizer = "SGD",
                        loss_function = "mse",
                        gamma = 0.01,
                        early_stopping_threshold = 5)

#Test-Data
input_test_test <- as.matrix(runif(1000, 0, 2*pi))
labels_test_test <- as.matrix(cos(input_train))

#test_nn mit Daten und netzwerk
loss_acc <- test_nn(trained_net_testing, input_test_test, labels_test_test, 100, "mse")

# Accuracy
test_that("Accuracy between 0 and 1", {
  expect_gte(loss_acc[2], 0)
  expect_lte(loss_acc[2], 1)
})

