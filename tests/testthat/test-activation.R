# Test Activation functions relu, sigmoid, softmax

test_that("Relu works", {
  expect_true(identical(activation("ReLu"),
                        fun <- function(x) {
                          x * (x > 0)
                        },ignore.environment = TRUE))
})

test_that("Sigmoid works", {
  expect_true(identical(activation("sigmoid"),
                        fun <- function(x) {
                          1/(1+exp(-x))
                        }, ignore.environment = TRUE))

})

test_that("Softmax works", {
  expect_true(identical(activation("softmax"),
                        fun <- function(x) {
                          (exp(x))/(sum(exp(x)))
                        }, ignore.environment = TRUE))

})


# Test Derivatives of activation Functions relu, sigmoid, softmax

test_that("Relu derivative works", {
  expect_true(identical(activation_derivative("ReLu"),
                        fun <- function(x) {
                          (x > 0) * 1
                        },ignore.environment = TRUE))
})

test_that("Sigmoid derivative works", {
  expect_true(identical(activation_derivative("sigmoid"),
                        fun <- function(x, ...) {
                          sigmoid_x <- 1/(1+exp(-x))
                          sigmoid_x * (1-sigmoid_x)
                        },ignore.environment = TRUE))
})

test_that("Softmax derivative works", {
  expect_true(identical(activation_derivative("softmax"),
                        fun <- function(x, ...){
                          1
                        },ignore.environment = TRUE))
})
