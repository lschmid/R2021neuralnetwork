from tkinter import *
from PIL import ImageGrab, Image
from datetime import datetime
import subprocess
import sys
import os


def install(pillow):
    subprocess.check_call([sys.executable, "-m", "pip", "install", pillow])


path = os.getcwd()


window = Tk()
window.title("Drawboard")
window.geometry("290x350")
window.resizable(0, 0)


def get_coordinates(event):
    global x_coord, y_coord
    x_coord, y_coord = event.x, event.y


def draw(event):
    global x_coord, y_coord
    drawboard.create_line((x_coord, y_coord, event.x, event.y), fill="white", width=35)
    x_coord, y_coord = event.x, event.y


def picture():
    drawboard.update()
    time_stamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    print(path + "\\drawboard\\digit_hd_" + time_stamp + ".jpg")
    x = window.winfo_x()+13
    y = window.winfo_y()+38
    x1 = x + 280
    y1 = y + 280
    ImageGrab.grab().crop((x, y, x1, y1)).save(path + "\\drawboard\\digit_hd_" + time_stamp + ".jpg")
    temp = Image.open(path + "\\drawboard\\digit_hd_" + time_stamp + ".jpg")
    resized = temp.resize((28, 28), Image.ANTIALIAS)
    resized.save(path + "\\drawboard\\digit_" + time_stamp + ".jpg")
    window.destroy()


drawboard = Canvas(window, bg="black", height=280, width=280)
button = Button(window, text="Ok", command=picture)
drawboard.pack(pady=5)
button.pack(side=BOTTOM, pady=15)

drawboard.bind("<Button-1>", get_coordinates)
drawboard.bind("<B1-Motion>", draw)


window.mainloop()
