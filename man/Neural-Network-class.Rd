% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/create_nn.R
\docType{class}
\name{Neural Network-class}
\alias{Neural Network-class}
\alias{.NeuralNet}
\title{Class neural network}
\description{
A S4-Object of class "Neural Network" with the parameters "values",
"values_before_activation", "structure", "type", "weights", "activation_fun"
and "activation_derivative".
}
